===========
pygpstools 0.1.3
===========

Set of tools for working with GPS, including:
* GPS time conversion
* GPS Almanacs parsing and interpreting
* Geodesy calculations


Typical usages:
=========

GPS time conversions:
-------------
A GPSTime object can be created in several different ways. A set of named parameters
can be passed to be interpreted, in such a way that a GPSTime is set from a local
time, UTC time, a gps time ammount expressed in seconds or split into week number
and time of week. Also, if no parameters are given, a GPSTime object will be created
from current UTC time::

    #!/usr/bin/env python

    from datetime import datetime
    from pygpstools import GPSTime

    gps_times = []
    gps_times.append(GPSTime(wn=1751, tow=314880))
    gps_times.append(GPSTime(gps_time=1059319680))
    gps_times.append(GPSTime(utc=datetime(2013, 7, 31, 15, 28, 0)))
    gps_times.append(GPSTime(localtime=datetime(2013, 7, 31, 17, 28, 0)))
    # Also possible to use struct_time instead of datetime
    gps_times.append(GPSTime())

    for gtime in gps_times:
        print 'GPS time: ' + str(gtime) + ' ---> ' + str(gtime.to_utc()) + \
            ' UTC (local time ' + str(gtime.to_localtime()) + ')'
			

Geodesy calculations:
-------------
The geodesy module is able to convert WGS84	ellipsoidal coordinates (latitude,
longitude and ellipsoidal height) into earth-centered earth-fixed (ECEF) coordinates
(X, Y, Z) and vice versa.

ECEF coordinates are given (and returned) in meters, and so is the WGS84 ellipsoidal
height. Latitude and longitude are operated in radians by default, but an optional
boolean parameter named ``rad`` can be provided to choose between radians and
degrees::

    #!/usr/bin/env python

    from pygpstools import geodesy

    x, y, z = geodesy.lla_to_ecef(lat=60, lon=30, ellipsoidal_height=0, rad=False)
	lat, lon, alt = ecef_to_lla(x, y, z)
	 
	 
Almanac parsing:
-------------	 
The almanac module gets the almanacs file provided by the US government from 
Internet, and is able to parse it and create Satellite objects with the almanac
data. A proxy can be set if needed::

    #!/usr/bin/env python

    from pygpstools import almanacs
	from pygpstools import GPSTime
    from pygpstools import Satellite

    almanacs.set_proxy({"http":"http://myproxy.com:8000"})
    almanacs.check_almanac_week()
    home_folder = expanduser("~")
    satellites = almanacs.read_gps_almanacs(almanac_path=home_folder \
     + '/current.alm')

    gps_time_now = GPSTime()
    for sat in satellites:
        if sat.get_position_from_almanacs(gps_time_now):
            print str(sat.prn + ': (' + str(sat.x) + ', ' + str(sat.y) + ', ' 
             + str(sat.z) + ') - [' + str(sat.vx) + ', ' + str(sat.vy) + ', ' \
             + str(sat.vz) + ']'