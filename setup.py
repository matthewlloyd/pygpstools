#!/usr/bin/env python
#
# Roman Rodriguez - Madrid - 2013

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
import os

packages=['pygpstools',
          'pygpstools.test']

setup(
    name='pygpstools',
    version='0.1.3',
    author='Roman Rodriguez',
    author_email='romanrdgz@gmail.com',
    url='https://bitbucket.org/romanrdgz/pygpstools',
    download_url='https://bitbucket.org/romanrdgz/pygpstools/downloads',
    description='Python set of tools for working with GPS, including GPS time conversion,\
				 GPS Almanacs parsing and interpreting and Geodesy calculations',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.txt'), 'r').read(),
    packages=packages,
    requires=[],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'License :: OSI Approved :: MIT License',
        'Topic :: Internet',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Scientific/Engineering :: GIS',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    keywords='gps satellite almanac geodesy ecef wgs84',
    license='LICENSE.txt',
)
