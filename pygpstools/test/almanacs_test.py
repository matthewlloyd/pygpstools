#!/usr/bin/env python

from pygpstools import almanacs
from pygpstools.gpstime import GPSTime

#almanacs.set_proxy({"http":"http://myproxyserver.com:8000"})
almanacs.check_almanac_week()
home_folder = expanduser("~")
almanacs.satellites = read_gps_almanacs(almanac_path=home_folder + '/current.alm')

gps_time_now = GPSTime()
for sat in satellites:
    if sat.get_position_from_almanacs(gps_time_now):
        print str(sat.prn + ': (' + str(sat.x) + ', ' + str(sat.y) + ', ' + str(sat.z) + ') - [' \
         + str(sat.vx) + ', ' + str(sat.vy) + ', ' + str(sat.vz) + ']'