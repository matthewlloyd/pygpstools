#!/usr/bin/env python

from os.path import expanduser
import urllib2
import constants
from gpstime import GPSTime
from satellite import Satellite


def _get_default_almanac_path():
    home_folder = expanduser("~")
    return home_folder + '/current.alm'


def read_gps_almanacs(almanac_path=None):
    '''
    This method is in charge of reading the almanacs from FILE
    '''
    if almanac_path == None:
        almanac_path = _get_default_almanac_path()

    satellites = []
    try:
        for section in load_almanac_sections(almanac_path):
            # Also insert a satellite object with this data into satellites list:
            satellites.append(Satellite(section))
    except IOError:
        raise Exception('ERROR: Unable to open almanacs file at the following path: ' \
            + str(constants.almanac_path))
    except KeyError as keyError:
        raise Exception('ERROR: Almanacs file seems to have an incorrect format: ' \
            + str(keyError))
    return satellites


def load_almanac_sections(almanac_path=None):
    '''
    This method parses a single almanac block from the almanac file and returns data as a dict
    '''
    if almanac_path == None:
        almanac_path = _get_default_almanac_path()

    with open(almanac_path, 'r') as infile:
        line = ''
        while True:
            while not line.startswith('****'): 
                line = next(infile)  # raises StopIteration, ending the generator
                continue  # find next entry

            entry = {}
            for line in infile:
                line = line.strip()
                if not line: break

                key, value = map(str.strip, line.split(':', 1))
                entry[key] = value

            yield entry


def get_almanacs(almanac_path=None):
    '''
    This method gets the almanacs file provided by the US government from Internet
    '''
    if almanac_path == None:
        almanac_path = _get_default_almanac_path()

    u = urllib2.urlopen(constants.almanac_source)
    
    print 'Saving new almanacs file into ' + almanac_path

    local_file = open(almanac_path, 'w')
    local_file.write(u.read())
    local_file.close()


def check_almanac_week(almanac_path=None):
    '''
    This method gets current gps week and checks almanacs file, in order to
    determine if it should be updated (and does so)
    '''
    almanac_week = 0
    try:
        with open(almanac_path, 'r') as local_file:
            first_line = local_file.readline()
            tokens = first_line.split()
    
            for i, token in enumerate(tokens):
                if token == 'Week':
                    almanac_week = int(tokens[i+1])
    except IOError:
        pass

    gps_time_now = GPSTime()
    current_week = gps_time_now.get_wn_rolled_over()

    if current_week > almanac_week:
        get_almanacs()


def set_proxy(proxy_dict):
    '''
    This method allows connections behind a proxy, to still be able
    to get almanacs from the Internet
    '''
    proxy_support = urllib2.ProxyHandler(proxy_dict)
    opener = urllib2.build_opener(proxy_support)
    urllib2.install_opener(opener)
