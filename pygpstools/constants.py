#!/usr/bin/env python

from math import pi

# Time conversions
seconds_one_week = 604800
gps_week_rollover = 1024
seconds_from_1970_to_1980 = 315532800
five_days = 432000
one_hour = 3600

# rad and deg conversions
RADTODEG = 180 / pi
DEGTORAD = pi / 180

# Phisic values
min_orbit_radius = 7000000  # Minimum value to check orbit semimajor axis correctness
mu_earth = 3.986005e14
omega_earth = 7.2921151467e-5
speed_of_light = 299792458.0
kepler_ecc_an_eq_max_iter = 25  # Max Iterations for convergence of Kepler's eccentric anomally equation
earth_radius = 6378137 # Earth radius
earth_ecc = 8.1819190842622e-2  # Earth eccentricity
flatte = 1/298.257223563
e2 = flatte * (2 - flatte)

# EPSILON values
eps = 0.00001
sat_epsilon = 1.e-7  # Below this value, certain values in SAT_POS are considered zero
kepler_epsilon = 1.e-14

# Satellite visibility settings
visible_elevation = 5  # degrees

# GPS constants
MAXNUMGPS = 32

# Internet resources
almanac_source = 'http://www.navcen.uscg.gov/?pageName=currentAlmanac&format=yuma'