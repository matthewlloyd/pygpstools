#!/usr/bin/env python

from math import sin, cos, acos, atan2, sqrt, pow
import constants


def lla_to_ecef(lat, lon, ellipsoidal_height, rad=True):
    '''
    This method changes from WGS84 ellipsoidal position into
    ECEF coord system.

    USAGE:
        x, y, z = lla_to_ecef(lat, lon, ellipsoidal_height, [opt]rad)

        x = ECEF X-coordinate (m)
        y = ECEF Y-coordinate (m)
        z = ECEF Z-coordinate (m)
        lat = geodetic latitude (radians by default)
        lon = longitude (radians by default)
        ellipsoidal_height = height above WGS84 ellipsoid (m)
        rad = True by default in order to operate with  lat and lon in 
              radians, False if lat and lon are given in degrees

    Notes:
        (1) This function assumes the WGS84 model.
        (2) Latitude is customary geodetic (not geocentric).
        (3) Tested but no warranty; use at your own risk.
    '''
    deg_to_rad = 1  # No need for conversion if working with radians
    if not rad:
        deg_to_rad = constants.DEGTORAD
    # First calculate repeated sin and cos
    sinlat = sin(deg_to_rad * lat)
    coslat = cos(deg_to_rad * lat)
    sinlon = sin(deg_to_rad * lon)
    coslon = cos(deg_to_rad * lon)

    N = constants.earth_radius / sqrt(1 - constants.e2 * sinlat**2)
    M = N + ellipsoidal_height

    x = M * coslat * coslon
    y = M * coslat * sinlon
    z = (N * (1 - constants.e2) + ellipsoidal_height) * sinlat
    return x, y, z


def ecef_to_lla(x, y, z, rad=True):
    '''
    This method converts earth-centered earth-fixed (ECEF)
    cartesian coordinates to latitude, longitude, and altitude
    from WGS84.

    USAGE:
        lat, lon, alt = ecef_to_lla(x, y, z, [opt]rad)

        lat = geodetic latitude (radians by default)
        lon = longitude (radians by default)
        alt = height above WGS84 ellipsoid (m)
        x = ECEF X-coordinate (m)
        y = ECEF Y-coordinate (m)
        z = ECEF Z-coordinate (m)
        rad = True by default in order to get lat and lon in radians,
              False if lat and lon should be returned in degrees

    Notes: 
        (1) This function assumes the WGS84 model.
        (2) Latitude is customary geodetic (not geocentric).
        (3) Tested but no warranty; use at your own risk.
        (4) Ported from Matlab code written by Michael Kleder (https://gist.github.com/klucar/1536054)
    '''
    asq = pow(constants.earth_radius, 2)
    esq = pow(constants.earth_ecc, 2)
    b = sqrt(asq * (1 - esq))
    bsq = pow(b, 2)
    ep = sqrt((asq - bsq) / bsq)
    p = sqrt(pow(x, 2) + pow(y, 2))
    th = atan2(a * z, b * p)

    lon = atan2(y, x)
    lat = atan2(z + pow(ep, 2) * b * pow(sin(th), 3), p - esq * a * pow(cos(th), 3))
    N = a / sqrt(1 - esq * pow(sin(lat), 2))
    alt = p / cos(lat) - N

    # mod lat to 0-2pi
    lon = lon % (2 * pi)

    # correction for altitude near poles left out.

    if not rad:
        lat = lat * constants.RADTODEG
        lon = lon * constants.RADTODEG

    return lat, lon, alt